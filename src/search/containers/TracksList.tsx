import React, { useEffect, useRef, useState } from "react";
import { Track } from "../../core/model/Search";

export interface Props {
  tracks: Track[];
}

export const TracksList = ({ tracks }: Props) => {
  const [activeTrack, setActiveTrack] = useState<Track | null>(null);
  const audioRef = useRef<HTMLAudioElement>(null);

  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    if (!audioRef.current) return;
    audioRef.current.volume = 0.1;

    audioRef.current.play();
  }, [activeTrack]);

  const play = async (track: Track) => {
    if (track === activeTrack && isPlaying) {
      audioRef.current?.pause();
    }
    setActiveTrack(track);
  };

  return (
    <div>
      <h4>{activeTrack?.name}</h4>

      <audio
        src={activeTrack?.preview_url}
        controls
        onPlay={() => setIsPlaying(true)}
        onPause={() => setIsPlaying(false)}
        className="w-100"
        ref={audioRef}
      ></audio>

      <div className="list-group">
        {tracks.map((track) => {
          return (
            <div
              className={
                "list-group-item" + (activeTrack === track ? " active" : "")
              }
              key={track.id}
              onClick={() => play(track)}
            >
              <span>{track.name}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};
