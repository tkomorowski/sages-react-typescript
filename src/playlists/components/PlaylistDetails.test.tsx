import { render, screen, logRoles, fireEvent } from "@testing-library/react";
import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistDetails } from "./PlaylistDetails";

describe("PlaylistDetails without data", () => {
  test('shows "Please select playlist" message', () => {
    render(<PlaylistDetails onEdit={() => {}} />);

    screen.getAllByText("Please select playlist");
  });
});

describe("PlaylistDetails with data", () => {
  //   beforeEach(() => {});
  const mockPlaylist = {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "OPis...",
  };

  const setup = ({ playlist = mockPlaylist }: { playlist?: Playlist }) => {
    const onEditSpy = jest.fn<void, [Playlist["id"]]>();

    render(<PlaylistDetails playlist={playlist} onEdit={onEditSpy} />);

    return { onEditSpy };
  };

  test("Renders playlist name", () => {
    setup({});

    const elem = screen.getByTestId("playlist-name");
    expect(elem).toHaveTextContent("Playlist 123");
  });

  test("Renders public playlist ", () => {
    setup({});

    const elem = screen.getByRole("definition", {
      name: "Playlist public",
    });
    expect(elem).toHaveTextContent("Yes");
    expect(elem).toHaveClass("text-danger");
    // logRoles(document.body)
  });

  test("Renders private playlist ", () => {
    setup({
      playlist: {
        ...mockPlaylist,
        public: false,
      },
    });

    const elem = screen.getByRole("definition", {
      name: "Playlist public",
    });
    expect(elem).toHaveTextContent("No");
    expect(elem).toHaveClass("text-success");
    // logRoles(document.body)
  });

  test("Emit edit event when 'edit' button clicked", () => {
    const { onEditSpy } = setup({});

    const btn = screen.getByRole("button", { name: "Edit" });
    // btn.click()
    // btn.dispatchEvent(new MouseEvent("click", { bubbles: true }));

    fireEvent.click(btn, {});

    // expect(onEditSpy).toHaveBeenCalledWith('234')
    expect(onEditSpy).toHaveBeenCalledWith<[string]>(mockPlaylist.id);
  });
});
