// tsrafc
import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlists: Playlist[];
  selected?: Playlist["id"];
  // onSelect: (id: string) => void;
  onSelect(id: string): void;
  onDelete?(id: string): void;
}

const cls = (...classes: (string | false)[]) =>
  classes.filter((c) => c !== false).join(" ");

export const PlaylistList = React.memo(
  ({ playlists, selected, onSelect, onDelete }: Props) => {
    console.log("PlaylistList render");

    return (
      <div>
        <div className="list-group">
          {playlists.map((playlist, index) => (
            <button
              role="tab"
              key={playlist.id}
              aria-selected={playlist.id === selected}
              onClick={() => onSelect(playlist.id)}
              className={cls(
                "list-group-item list-group-item-action ",
                playlist.id === selected && "active"
              )}
            >
              <span>
                {index + 1}. {playlist.name}
              </span>
              {onDelete && (
                <span
                  role="button"
                  aria-label="Remove Playlist"
                  className="close float-end"
                  onClick={() => onDelete(playlist.id)}
                >
                  &times;
                </span>
              )}
            </button>
          ))}
        </div>
      </div>
    );
  }
  // /* propsAreEqual */
  // ,(prevProps: Readonly<Props>, nextProps: Readonly<Props>) => {
  //   return (
  //     prevProps.playlists === nextProps.playlists &&
  //     prevProps.selected === nextProps.selected
  //   );
  // }
);

/*  <div>
      <div className="list-group">
        {playlists.map((playlist, index) => {
          return (
            <button
              type="button"
              // onClickCapture={}
              onClick={(event) => {
                onSelect(playlist.id);
              }}
              className={cls(
                "list-group-item list-group-item-action",
                playlist.id === selected && "active"
              )}
              key={playlist.id}
            >
              <span>
                {index + 1}. {playlist.name}
              </span>
              {onDelete && (
                <span
                  className="close float-end"
                  // onClickCapture={}
                  onClick={(event) => {
                    event.stopPropagation();
                    onDelete(playlist.id);
                  }}
                >
                  &times;
                </span>
              )}
            </button>
          );
        })}
      </div>
    </div>
   */
