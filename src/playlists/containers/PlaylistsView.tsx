import { useCallback, useEffect, useMemo, useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistList } from "../components/PlaylistList";
import { Route, useHistory } from "react-router-dom";
import { playlistsMock } from "../../core/mocks/playlistsMock";
interface Props {}

export const PlaylistsView = (props: Props) => {
  const [playlists, setPlaylists] = useState(playlistsMock);
  const [selectedId, setSelectedId] = useState<string | undefined>();
  const [selectedPlaylist, setSelectedPlaylist] = useState<
    Playlist | undefined
  >();
  const { push } = useHistory();

  useEffect(() => {
    setSelectedPlaylist(playlists.find((p) => p.id === selectedId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedId, playlists]);

  const selectPlaylist = useCallback(
    (id: Playlist["id"]) => {
      push(`/playlists/${id}/details`);
    },
    [push]
  );

  const cancel = useCallback(() => push(`/playlists/${selectedId}/details`), [
    selectedId,
    push,
  ]);

  const saveChanges = useCallback(
    (szkic: Playlist) => {
      setPlaylists((playlists) =>
        playlists.map((p) => (p.id === szkic.id ? szkic : p))
      );
      push(`/playlists/${szkic.id}/details`);
    },
    [push]
  );

  const deletePlaylist = useCallback(
    (id: Playlist["id"]) => {
      setPlaylists(playlists.filter((p) => p.id !== id));
    },
    [playlists]
  );

  const [nextId, setNextId] = useState("");

  const createNewPlaylist = useCallback(() => {
    setNextId(Math.ceil(Date.now() + Math.random() * 100_000).toString());
    push(`/playlists/create`);
  }, []);

  const addPlaylist = useCallback((draft: Playlist) => {
    // setPlaylists( [...playlists, draft]);
    setPlaylists((playlists) => [...playlists, draft]);
    setSelectedId(draft.id);
    push(`/playlists/${selectedId}/details`);
  }, []);
  // }, [playlists]);

  const emptyPlaylist: Playlist = useMemo(
    () => ({
      id: nextId,
      type: "playlist",
      name: "",
      description: "",
      public: false,
    }),
    [nextId]
  );

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selected={selectedPlaylist?.id}
            onSelect={selectPlaylist}
            onDelete={deletePlaylist}
          />
          <button
            className="btn btn-info float-end mt-4"
            onClick={createNewPlaylist}
          >
            Create new playlist
          </button>
        </div>

        <div className="col">
          <Route
            path="/playlists/create"
            render={() => (
              <PlaylistForm
                playlist={emptyPlaylist}
                onCancel={cancel}
                onSave={addPlaylist}
              />
            )}
          />

          <Route
            path="/playlists/:playlist_id/details"
            render={({ match }) => {
              setSelectedId(match.params.playlist_id);
              return (
                selectedPlaylist && (
                  <PlaylistDetails
                    playlist={selectedPlaylist}
                    onEdit={() => push(`/playlists/${selectedId}/edit`)}
                  />
                )
              );
            }}
          />

          <Route
            path="/playlists/:playlist_id/edit"
            render={({ match }) => {
              setSelectedId(match.params.playlist_id);
              return (
                selectedPlaylist && (
                  <PlaylistForm
                    playlist={selectedPlaylist}
                    onCancel={cancel}
                    onSave={saveChanges}
                  />
                )
              );
            }}
          />

          {!selectedPlaylist && (
            <p className="alert alert-info">Please select playlist</p>
          )}
        </div>
      </div>
    </div>
  );
};

// const ul = <dl>
//     {true && <Fragment>
//         <dt></dt>
//         <dd></dd>
//     </Fragment>}
// </dl>

// const ul = <dl>
//     {true && <>
//         <dt></dt>
//         <dd></dd>
//     </>}
// </dl>
