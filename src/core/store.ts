import { createStore, combineReducers, applyMiddleware, Middleware, Dispatch } from 'redux'
import * as playlists from './reducers/Playlists'

import logger from 'redux-logger'
import thunk, { ThunkDispatch } from 'redux-thunk'

const reducer = combineReducers({
    [playlists.featureKey]: playlists.reducer,
    // [search.featureKey]: search.reducer,
    // [counter.featureKey]: counter.reducer,
    // [users.featureKey]: users.reducer,
})

export type AppState = ReturnType<typeof reducer>


// const logger: Middleware = (api) => (next) => (action) => {
//     console.log(action);
//     next(action)
//     console.log(api.getState());
// }

// const thunk: Middleware = (api) => (next) => (action) => {
//     if (typeof action === 'function') {
//         action(api.dispatch)
//     } else {
//         next(action)
//     }
// }

export const store = createStore(reducer, applyMiddleware(thunk,logger))

export type AppDispatch = ThunkDispatch<AppState, {}, Parameters<typeof store.dispatch>[0]>