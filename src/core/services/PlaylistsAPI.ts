import axios from "axios";
import { Playlist } from "../model/Playlist";
import { PagingObject } from "../model/Search";

export { }


const playlistsMock: Playlist[] = [
    { id: "123", type: 'playlist', name: "Playlist 123", public: true, description: "OPis..." },
    { id: "234", type: 'playlist', name: "Playlist 234", public: false, description: "OPis..." },
    { id: "345", type: 'playlist', name: "Playlist 345", public: true, description: "OPis..." },
];


export const fetchPlaylists = async () => {

    const { data: { items } } = await axios.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/me/playlists`)
    return items
}

export const fetchPlaylistById = async (id: string) => {
    return playlistsMock.find(p => p.id === id)
}

// fetchPlaylists().then(p => p[0].name)