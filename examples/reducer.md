```ts
[1,2,3].map( x => x * 2)
(3) [2, 4, 6]

[1,2,3].filter( x => x % 2 == 0)
[2]

[1,2,3,4,5].reduce((sum,x)=>{
    console.log(sum,x)
    return sum + x 
},0)    

VM3780:2 0 1
VM3780:2 1 2
VM3780:2 3 3
VM3780:2 6 4
VM3780:2 10 5
15

[1,2,3,4,5].reduce((state,x)=>{
    return { ...state, counter: state.counter + x } 
},{counter:0, todos:[]})   


inc = (payload = 1) => ({ type:'INC', payload });
dec = (payload = 1) => ({ type:'DEC', payload });
addTodo = (payload = {name:'new todo'} ) => ({ type:'ADD_TODO', payload });


[inc(1), inc(2),dec(1), addTodo({name:'buy milk'}), inc(1) ].reduce((state,action)=>{
    switch(action.type){
        case 'INC' : 
            return { ...state, counter: state.counter + action.payload } 
        case 'DEC' : 
            return { ...state, counter: state.counter - action.payload } 
        case 'ADD_TODO' : 
            return { ...state, todos: [...state.todos, action.payload ]  } 
    }
},{counter:0, todos:[]})    
// {counter: 3, todos: Array(1)}
// counter: 3
// todos: Array(1)
//      0: {name: "buy milk"}


inc = (payload = 1) => ({ type:'INC', payload });
dec = (payload = 1) => ({ type:'DEC', payload });
addTodo = (payload = {name:'new todo'} ) => ({ type:'ADD_TODO', payload });


reduceCounter = (state = 0 ,action) => {
    switch(action.type){
        case 'INC' : 
            return  state + action.payload 
        case 'DEC' : 
            return state + action.payload  
        default: 
            return state;
    }
}

reducer = (state,action) => {
    state = {
        ...state,
        subcounter: reduceCounter(state.subcounter, action) 
    };

    switch(action.type){
        case 'INC' : 
            return { ...state, counter: state.counter + action.payload } 
        case 'DEC' : 
            return { ...state, counter: state.counter - action.payload } 
        case 'ADD_TODO' : 
            return { ...state, todos: [...state.todos, action.payload ]  } 
        default: 
            return state
    }
}

state = {counter:0, todos:[], subcounter:0 };
state = reducer(state, inc(1) )
state = reducer(state, inc(2) )
state = reducer(state, dec(1) )
state = reducer(state, addTodo({name:'buy milk'}) ) 
  
{counter: 2, todos: Array(1), subcounter: 4}
```