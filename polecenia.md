# GIT
cd ..
git clone https://bitbucket.org/ev45ive/sages-react-typescript.git sages-react-typescript
cd sages-react-typescript
npm i 
npm start

## GIT Update
git stash -u 
git pull 
npm i 

## Create react app 
https://create-react-app.dev/docs/adding-typescript/#getting-started-with-typescript-and-react

npx create-react-app . --template typescript

npm start
## VS Code extensions
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

## Chrome extensions
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi


## UI toolkit
https://material-ui.com/
https://ant.design/components/overview/
https://react-bootstrap.github.io/components/alerts 

https://chakra-ui.com/docs/form/button

https://retool.com/pricing/
https://www.telerik.com/kendo-react-ui/pricing/ 
https://js.devexpress.com/Buy/ 

## Bootstrap CSS
https://getbootstrap.com/docs/5.0/getting-started/download/#npm

## Playlists
mkdir -p src/playlists/containers
touch src/playlists/containers/PlaylistsView.tsx

mkdir -p src/playlists/components
touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistForm.tsx

## Storybook
npx -p @storybook/cli sb init --story-format=csf-ts
 npm run storybook


## Testing
https://jestjs.io/docs/getting-started 
https://github.com/jsdom/jsdom

https://testing-library.com/docs/queries/about
https://testing-library.com/docs/dom-testing-library/api-events

https://testing-library.com/docs/react-testing-library/migrate-from-enzyme/

https://reactjs.org/docs/test-utils.html#act
https://testing-library.com/docs/guide-disappearance/

https://jestjs.io/docs/troubleshooting#debugging-in-vs-code
node --inspect-brk ./node_modules/jest/bin/jest.js --runInBand


## Search module

mkdir -p src/search/containers
touch src/search/containers/AlbumsSearch.tsx

mkdir -p src/search/components
touch src/search/components/SearchForm.tsx
touch src/search/components/ResultsGrid.tsx
touch src/search/components/AlbumCard.tsx

echo 'export {}' > src/core/model/Search.tsx
echo 'export {}' > src/core/services/SearchAPI.tsx

## Quicktype
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype
https://developer.spotify.com/documentation/web-api/reference/#category-albums

## OAuth
https://www.npmjs.com/package/react-oauth2-hook
npm i react-oauth2-hook immutable  react-storage-hook  prop-types


## Parsing / Validation / pattern matching
https://github.com/colinhacks/zod
https://github.com/gvergnaud/ts-pattern

## Custom hooks
https://use-http.com/#/?id=basic-usage-auto-managed-state
https://swr.vercel.app/docs/conditional-fetching

## React Router
https://reactrouter.com/
npm install react-router-dom 

Try `npm i --save-dev @types/react-router-dom` if it exists or add a new declaration (.d.ts) file containing `declare module 'react-router-dom';`ts(7016)

## Ambient module definition

// /// <reference path="../index.d.ts"/>
// https://www.typescriptlang.org/docs/handbook/modules.html#ambient-modules
declare module 'react-router-dom' {

## Album Details - excercise
touch src/search/containers/AlbumDetails.tsx

1. Add To Router
Route /search/albums?id=5Tby0U5VndHW0SomYO7Id7

2. Show ID param

3. Fetch data by id
src\core\services\SearchAPI.tsx  getAlbumById(album.id) 

4. Show details - name, artists[0].name

5. Show album card
<AlbumCard album={album}>


## Local configiration

https://create-react-app.dev/docs/adding-custom-environment-variables/

cp example.env .env.local

## Redux
https://redux.js.org/
https://react-redux.js.org/introduction/getting-started

npm i redux react-redux @types/react-redux

https://github.com/reduxjs/reselect

https://redux-toolkit.js.org/tutorials/typescript

## Middleware

npm i redux-logger redux-thunk @types/redux-logger